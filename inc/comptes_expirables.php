<?php

namespace Spip;

/**
 * Classe ComptesExpirables
 */
class ComptesExpirables {

	// Config
	public static $config;

	// Identifiants des chaîne de langue donnant la raison de la présence d'une date d'expiration
	const RAISON_DATE_MANUELLE          = 'raison_date_manuelle';
	const RAISON_ECHEANCE_INACTIF       = 'raison_echeance_inactif';
	const RAISON_ECHEANCE_INACTIF_JOURS = 'raison_echeance_inactif_jours';
	const RAISON_ECHEANCE_CREATION      = 'raison_echeance_creation';

	/**
	 * Constructeur
	 */
	public function __construct() {
		self::set_config();
	}

	/**
	 * Retourne le WHERE d'une requête sql pour sélectionner les comptes en fonction de critères liés à l'expiration
	 *
	 * @note
	 * Pas possible d'utiliser un alias du select dans le WHERE (`age_en_ligne` par ex.), obligé de répéter la formule.
	 * C'est possible dans un having, mais c'est censé être moins bon pour la perf
	 *
	 * @param string $quoi
	 *    Identifiant de la sélection qu'on souhaite :
	 *
	 *    - expirables             : tous ceux non webmestres dont le statut est compatible avec une date d'expiration
	 *    - retrogrades_oui        : date dépassée + statut expiré
	 *    - retrogrades_non        : inverse
	 *    - en_ligne_oui           : ceux qui se sont connectés, optionnellement il y a moins de N jours (si âge max configuré)
	 *    - en_ligne_non           : ceux jamais connectés ou optionnellement connectés il y a trop longtemps (si âge max configuré)
	 *    - statut_expire_oui      : avec le statut expiré
	 *    - statut_expire_non      : sans le statut expiré
	 *    - date_expiration_oui    : avec date d'expiration
	 *    - date_expiration_non    : sans date d'expiration
	 *    - date_expiration_future : avec date d'expiration future
	 *    - date_expiration_passee : avec date d'expiration passée
	 * @param string|null $table
	 *    Nom de la table ou son alias dans la requête
	 * @param array $options
	 *     Options selon les critères :
	 *     - date (string) : date de référence
	 *     - age (int) : nombre de jours
	 *     - expires (bool)
	 *     - expirables (bool)
	 * @return string
	 */
	public function get_where(string $quoi, ? string $table = null, array $options = []) : string
	{
		$where              = '';
		$table              = ($table ? "$table." : ''); // alias de table
		$config             = self::$config;
		// Statuts configurés
		$statuts_expirables = self::get_statuts_expirables();
		$statuts_expires    = self::get_statuts_expires();
		$statuts_tous       = self::get_statuts();
		// Divers
		$date_sql_null    = '0000-00-00 00:00:00';
		// Formule pour avoir le nombre de jours depuis la dernière connexion (NULL si aucune connexion)
		$age_en_ligne_max = ($options['age'] ?? $config['age_en_ligne_max']); // nombre de jours
		$date             = (isset($options['date']) ? sql_quote($options['date']) : 'NOW()'); // date de référence
		$age_en_ligne     = "CASE WHEN ${table}en_ligne = " . sql_quote($date_sql_null) . " THEN NULL ELSE TIMESTAMPDIFF(HOUR, ${table}en_ligne, $date)/24 END";

		switch ($quoi) {

			// Expirables : tous ceux susceptibles d'avoir une date d'expiration
			// C.a.d les non webmestres avec des statuts expirables ou expirés
			// Optionnellement on peut restreindre à l'un ou l'autre avec les options `expirables` et `expires`
			case 'expirables':
				$where_expirables = [];
				// Pas les webmestres
				$where_expirables[] = "(${table}webmestre != " . sql_quote('oui') . ')';
				// Les statuts expirables ou expirés, optionnellement que les uns ou les autres
				$expirables = ($options['expirables'] ?? true);
				$expires    = ($options['expires'] ?? true);
				if ($expirables or $expires) {
					$statuts    = [];
					if ($expirables and $expires) {
						$statuts = $statuts_tous;
					} elseif ($expirables) {
						$statuts = $statuts_expirables;
					} elseif ($expires) {
						$statuts = $statuts_expires;
					}
					$where_expirables[] = sql_in("${table}statut", $statuts);
				}
				$where = implode(' AND ', $where_expirables);
				break;

			// ---------------------------
			// RETROGRADES (statut + date)
			// ---------------------------

			// Rétrogradés : avec un statut expiré ET une date dépassée
			case 'retrogrades_oui':
				$where_retrogrades  = [
					sql_in("${table}statut", $statuts_expires),
					"${table}date_expiration IS NOT NULL",
					"${table}date_expiration != " . sql_quote($date_sql_null),
					"${table}date_expiration < NOW()",
				];
				$where = implode(' AND ', $where_retrogrades);
				break;

			// Non rétrogradés : inverse du précédent
			case 'retrogrades_non':
				$where_retrogrades  = [
					sql_in("${table}statut", $statuts_expires, 'NOT'),
					"${table}date_expiration IS NULL",
					"${table}date_expiration >= NOW()",
				];
				$where = implode(' OR ', $where_retrogrades);
				break;

			// ------
			// STATUT
			// ------

			// Expirés : avec un statut expiré
			case 'statut_expire_oui':
				$where = sql_in("${table}statut", $statuts_expires);
				break;

			// Non expirés : avec un statut autre que expiré
			case 'statut_expire_non':
				$where = sql_in("${table}statut", $statuts_expires, 'NOT');
				break;

			// -----------------
			// DATE D'EXPIRATION
			// -----------------

			// Ceux sans date d'expiration, ou optionnellement avec mais plus lointaine qu'une date donnée
			case 'date_expiration_non':
				$where = "${table}date_expiration IS NULL OR ${table}date_expiration = " . sql_quote($date_sql_null);
				if (!empty($options['date_min'])) {
					$where_date_min = "${table}date_expiration > " . sql_quote($options['date_min']);
					$where = "($where) OR ($where_date_min)";
				}
				break;

			// Ceux avec une date d'expiration
			case 'date_expiration_oui':
				$where = "${table}date_expiration IS NOT NULL AND ${table}date_expiration != " . sql_quote($date_sql_null);
				break;

			// Ceux avec une date d'expiration dans le futur
			case 'date_expiration_future':
				$where = "${table}date_expiration > NOW()";
				break;

			// Ceux avec une date d'expiration dépassée
			case 'date_expiration_passee':
				$where_date_expiree  = [
					"${table}date_expiration IS NOT NULL",
					"${table}date_expiration != " . sql_quote($date_sql_null),
					"${table}date_expiration <= NOW()",
				];
				$where = implode(' AND ', $where_date_expiree);
				break;

			// --------
			// ACTIVITE
			// --------

			// Inactifs : jamais connectés ou connectés il y a plus de N jours (optionnel, dépend de la config)
			case 'en_ligne_non':
				// Les comptes jamais connectés
				$where = "$age_en_ligne IS NULL";

				// S'il y a un âge de connexion max, on ajoute ceux qui l'ont dépassé.
				if ($age_en_ligne_max > 0) {
					$where_age = "$age_en_ligne > $age_en_ligne_max";
					$where = "($where) OR ($where_age)";
				}
				break;

			// Actifs : connectés au moins 1 fois, ou il y a moins de N jours (optionnel, dépend de la config)
			case 'en_ligne_oui':
				// Soit il y a un âge de connexion max.
				if ($age_en_ligne_max > 0) {
					$where = "$age_en_ligne <= $age_en_ligne_max";
				// Soit connectés au moins 1 fois
				} else {
					$where = "$age_en_ligne IS NOT NULL";
				}
				break;
		}

		// On parenthétise
		$where = ($where ? "($where)" : $where);

		return $where;
	}

	/**
	 * Lister tous les statuts de la config : expirables et expirés
	 *
	 * @return array
	 *     Tableau linéaire avec le code des statuts
	 */
	public static function get_statuts() : array
	{
		$statuts_expirables = self::get_statuts_expirables();
		$statuts_expires    = self::get_statuts_expires();
		$statuts_tous       = array_unique(array_merge($statuts_expirables, $statuts_expires));

		return $statuts_tous;
	}

	/**
	 * Lister tous les statuts expirables de la config
	 * 
	 * C'est à dire ceux qui ont un statut expiré de configuré
	 *
	 * @return array
	 *     Tableau linéaire avec le code des statuts
	 */
	public static function get_statuts_expirables() : array
	{
		$statuts_expirables = array_keys(array_filter(self::$config['statuts']));
		return $statuts_expirables;
	}

	/**
	 * Lister tous les statuts expirés de la config
	 *
	 * @return array
	 *     Tableau linéaire avec le code des statuts
	 */
	public static function get_statuts_expires() : array
	{
		$statuts_expires = array_unique(array_values(array_filter(self::$config['statuts'])));
		return $statuts_expires;
	}

	/**
	 * Donne le statut expiré configuré pour un statut
	 *
	 * @param string $statut
	 *     Code du statut de référence
	 * @return string
	 *     Code du statut, chaîne vide si aucun
	 */
	public static function get_statut_expire($statut) : string
	{
		$statut_expire = (self::$config['statuts'][$statut] ?? '');
		return $statut_expire;
	}

	/**
	 * Set la config
	 *
	 * Met des valeurs par défaut si on n'a pas encore configuré le plugin
	 * Nb : un seul appel pour toutes les instances
	 *
	 * @return void
	 */
	private static function set_config() : void
	{

		if (!isset(self::$config)) {
			include_spip('inc/config');

			// La config enregistrée en bdd
			$config_bdd = lire_config('comptes_expirables', []);
			$config_bdd = array_filter($config_bdd);

			// Les valeurs par défaut
			$statuts_defaut = [
				'0minirezo' => '7desactive',
				'1comite'   => '7desactive',
				'6forum'    => '7desactive',
			];
			$config_defaut = [
				'statuts'          => ($config_bdd['statuts'] ?? $statuts_defaut),
				'age_en_ligne_max' => null,
				'age_nouveaux_max' => null,
				'notifications'    => [],
				'email_admins'     => $GLOBALS['meta']['email_webmaster'],
			];

			// On merge
			$config = array_merge($config_defaut, $config_bdd);
			self::$config = $config;
		}
	}

}

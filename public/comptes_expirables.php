<?php
/**
 * Critères et balises utiles au plugin Comptes expirables
 *
 * @plugin     Comptes expirables
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Comptes_expirables\Public
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


// ========
// CRITERES
// ========


/**
 * Fonction privée pour calculer les différents critères
 *
 * Fait l'interface pour choisir quel WHERE utiliser.
 *
 * @param string $quoi
 *     Type de WHERE : expirables, statut_expire, date_expiration, en_ligne
 * @param bool $suffixer_quoi
 *     si true, alors il faut suffixer le quoi avec la valeur du param ou celle de l'env
 * @param string|null $valeur_parametre
 *     Valeur du paramètre éventuel
 * @param string|null $valeur_env
 *     Valeur de l'homonyme du critère dans l'environnement
 * @param bool $op_condition
 *     `true` s'il y a l'opérateur conditionnel : `{? critere}`
 * @param bool $inverser
 *     `true` s'il y a l'opérateur pour inverser la sélection : `{! critere}`
 * @param string $id_table
 *     Nom ou alias de la table dans la requête
 * @param array $options
 *     Tableau d'options éventuelles
 * @return string
 *     Contenu du WHERE ou chaîne vide
 */
function comptes_expirables_calculer_critere(
	string $quoi,
	bool $suffixer_quoi,
	? string $valeur_parametre,
	? string $valeur_env,
	bool $op_condition,
	bool $op_inverser,
	string $id_table,
	array $options = []
) : string {

	$where  = '';
	$gogogo = false;

	// Si pas besoin de suffixer le $quoi, on vérifie juste la condition
	if (!$suffixer_quoi) {
		$gogogo = (!$op_condition or ($op_condition and !empty($valeur_env)));

	// Sinon on vérifie qu'on a une valeur connue à utiliser en suffixe du $quoi
	} else {

		// Valeurs possibles
		$valeurs_oui       = ['oui', '1'];
		$valeurs_non       = ['non', '-1'];
		$valeurs_autres    = ['sans', 'future', 'passee'];
		$valeurs_possibles = array_merge($valeurs_oui, $valeurs_non, $valeurs_autres);

		// Valeur à utiliser : soit celle en paramètre, soit celle de l'env en cas d'opérateur conditionnel
		$valeur = ($op_condition ? $valeur_env : $valeur_parametre);

		// Remplacer certains alias (ex. : 1 → oui), puis suffixer le $quoi
		if ($gogogo = in_array($valeur, $valeurs_possibles)) {
			if (in_array($valeur, $valeurs_oui)) {
				$valeur = $valeurs_oui[0];
			}
			if (in_array($valeur, $valeurs_non)) {
				$valeur = $valeurs_non[0];
			}
			$quoi .= "_$valeur"; // par exemple : statut_expire → statut_expire_oui
		}
	}

	// Go go go
	if ($gogogo ) {
		include_spip('inc/comptes_expirables');
		$comptes = new \Spip\ComptesExpirables;
		$where = $comptes->get_where($quoi, $id_table, $options);
		if ($op_inverser) {
			$where = "NOT ($where)";
		}
	}

	return $where;
}


/**
 * Critère {comptes_expirables} : sélectionne les comptes concernés par les dates d'expiration
 *
 * C'est à dire :
 * - tout ceux ayant un statut pouvant recevoir une date d'expiration ou déjà expirés
 * - et non webmestres
 *
 * Dépend des statuts configurés
 *
 * @note
 * Différent de `{comptes_date_expirable}`
 *
 * @example
 * `<BOUCLE_s(AUTEURS) {comptes_expirables}>`
 * `<BOUCLE_s(AUTEURS) {comptes_expirables ?}>`
 * `<BOUCLE_s(AUTEURS) {! comptes_expirables}>`
 *
 * @param string $idb
 * @param object $boucles
 * @param object $crit
 * @return void
 */
function critere_AUTEURS_comptes_expirables_dist($idb, &$boucles, $crit) {
	$boucle       = &$boucles[$idb];
	$id_table     = $boucle->id_table;
	$op_inverser  = !empty($crit->not);
	$op_condition = !empty($crit->cond);
	$_env         = '(@$Pile[0]["comptes_statut_expire"] ?? null)';
	$_parametre   = 'null';
	$_options     = '[]';

	// On lèves les restrictions par défaut : pas besoin de restreindre aux auteurs ayant publié, ni les statuts
	$boucle->modificateur['tout'] = true;
	$boucle->modificateur['statut'] = true;

	// S'il y a le drapeau indiquant qu'un autre critères s'occupe du statut, on ne s'en occupe pas
	/*if ($boucle->modificateur['comptes_expirables_statut'] ?? false) {
		$_options = "['expirables' => false, 'expires' => false]";
	}*/

	$where = "comptes_expirables_calculer_critere('expirables', false, $_parametre, $_env, '$op_condition', '$op_inverser', '$id_table', $_options)";
	$boucle->where[] = $where;
}

/**
 * Critère {comptes_statut_expire #ENV{choix}} : sélectionne les comptes avec le statut expiré ou non
 *
 * Dépend du statut configuré
 *
 * Nécessite un paramètre ou le nom du critère dans l'env avec une de ces valeurs :
 * - oui | 1 : pour prendre ceux avec le statut
 * - non | -1 : pour prendre ceux sans le statut
 *
 * @example
 * `<BOUCLE_x(AUTEURS) {comptes_expires #ENV{choix}}>`
 * `<BOUCLE_x(AUTEURS) {comptes_expires ?}>`
 *
 * @param string $idb
 * @param object $boucles
 * @param object $crit
 * @return void
 */
function critere_AUTEURS_comptes_statut_expire_dist($idb, &$boucles, $crit) {
	$boucle       = &$boucles[$idb];
	$id_table     = $boucle->id_table;
	$op_inverser  = !empty($crit->not);
	$op_condition = !empty($crit->cond);
	$_env         = '(@$Pile[0]["comptes_statut_expire"] ?? null)';
	$_parametre   = (isset($crit->param[0]) ?
		calculer_liste($crit->param[0], [], $boucles, $boucles[$idb]->id_parent)
		: "''");

	// Un drapeau pour indiquer au critère {comptes_expirables} qu'on s'occupe déjà du statut
	//$boucle->modificateur['comptes_expirables_statut'] = true;

	$where = "comptes_expirables_calculer_critere('statut_expire', true, $_parametre, $_env, '$op_condition', '$op_inverser', '$id_table')";
	$boucle->where[] = $where;
}

/**
 * Critère {comptes_date_expiration #ENV{choix}} : sélectionne les comptes en fonction des dates d'expiration
 *
 * Nécessite un paramètre ou le nom du critère dans l'env avec une de ces valeurs :
 * - oui | 1  : ceux avec une date
 * - non | -1 : ceux sans date
 * - future   : ceux avec une date future
 * - passee   : ceux avec une date passée
 *
 * @example
 * `<BOUCLE_x(AUTEURS) {comptes_date_expiration #ENV{choix}}>`
 * `<BOUCLE_x(AUTEURS) {comptes_date_expiration ?}>`
 *
 * @param string $idb
 * @param object $boucles
 * @param object $crit
 * @return void
 */
function critere_AUTEURS_comptes_date_expiration_dist($idb, &$boucles, $crit) {
	$boucle       = &$boucles[$idb];
	$id_table     = $boucle->id_table;
	$op_inverser  = !empty($crit->not);
	$op_condition = !empty($crit->cond);
	$_env         = '(@$Pile[0]["comptes_date_expiration"] ?? null)';
	$_parametre   = (isset($crit->param[0]) ?
		calculer_liste($crit->param[0], [], $boucles, $boucles[$idb]->id_parent)
		: "''");

	$where = "comptes_expirables_calculer_critere('date_expiration', true, $_parametre, $_env, '$op_condition', '$op_inverser', '$id_table')";
	$boucle->where[] = $where;
}

/**
 * Critère {comptes_en_ligne #ENV{choix}} : sélectionne les comptes en fonction de leur activité
 *
 * Dépend de la durée d'inactivité maximale configurée
 *
 * Nécessite un paramètre ou le nom du critère dans l'env avec une de ces valeurs :
 * - oui | 1  : ceux actifs
 * - non | -1 : ceux inactifs
 *
 * @example
 * `<BOUCLE_x(AUTEURS) {comptes_en_ligne #ENV{choix}}>`
 * `<BOUCLE_x(AUTEURS) {comptes_en_ligne ?}>`
 *
 * @param string $idb
 * @param object $boucles
 * @param object $crit
 * @return void
 */
function critere_AUTEURS_comptes_en_ligne_dist($idb, &$boucles, $crit) {
	$boucle       = &$boucles[$idb];
	$id_table     = $boucle->id_table;
	$op_inverser  = !empty($crit->not);
	$op_condition = !empty($crit->cond);
	$_env         = '(@$Pile[0]["comptes_en_ligne"] ?? null)';
	$_parametre   = (isset($crit->param[0]) ?
		calculer_liste($crit->param[0], [], $boucles, $boucles[$idb]->id_parent)
		: "''");

	$where = "comptes_expirables_calculer_critere('en_ligne', true, $_parametre, $_env, '$op_condition', '$op_inverser', '$id_table')";
	$boucle->where[] = $where;
}

/**
 * Critère {comptes_retrogrades #ENV{choix}} : sélectionne les comptes rétrogradés ou non
 *
 * Comptes rétrogradés : date dépassée + statut expiré
 * Dépend du statut configuré
 *
 * Nécessite un paramètre ou le nom du critère dans l'env avec une de ces valeurs :
 * - oui | 1  : ceux rétrogradés
 * - non | -1 : les autres
 *
 * @example
 * `<BOUCLE_x(AUTEURS) {comptes_retrogrades #ENV{choix}}>`
 * `<BOUCLE_x(AUTEURS) {comptes_retrogrades ?}>`
 *
 * @param string $idb
 * @param object $boucles
 * @param object $crit
 * @return void
 */
function critere_AUTEURS_comptes_retrogrades_dist($idb, &$boucles, $crit) {
	$boucle       = &$boucles[$idb];
	$id_table     = $boucle->id_table;
	$op_inverser  = !empty($crit->not);
	$op_condition = !empty($crit->cond);
	$_env         = '(@$Pile[0]["comptes_retrogrades"] ?? null)';
	$_parametre   = (isset($crit->param[0]) ?
		calculer_liste($crit->param[0], [], $boucles, $boucles[$idb]->id_parent)
		: "''");

	// Un drapeau pour indiquer au critère {comptes_expirables} qu'on s'occupe déjà du statut
	//$boucle->modificateur['comptes_expirables_statut'] = true;

	$where = "comptes_expirables_calculer_critere('retrogrades', true, $_parametre, $_env, '$op_condition', '$op_inverser', '$id_table')";
	$boucle->where[] = $where;
}


// =======
// BALISES
// =======


/**
 * Indique si un compte est rétrogradé
 *
 * @param [type] $p
 * @return void
 */
function balise_COMPTE_RETROGRADE_dist($p) {

	$index        = index_boucle($p); // <BOUCLE_truc> → _truc
	$boucle       = ($index ? $p->boucles[$index] : null);
	$objet_boucle = ($boucle ? $boucle->type_requete : ''); // articles
	$cle_objet    = ($boucle ? $p->boucles[$index]->primary : ''); // id_article
	$serveur      = ($boucle ? $p->boucles[$index]->sql_serveur : '');
	$etoile       = $p->etoile;

	if (
		$boucle
		and $objet_boucle === 'auteurs'
	) {
		$_id_auteur         = champ_sql($cle_objet, $p);
		$_statut            = champ_sql('statut', $p);
		$_raison_expiration = champ_sql('raison_expiration', $p);
		$_date_expiration   = champ_sql('date_expiration', $p);
		$code = "calculer_balise_compte_retrograde_dist($_statut, $_date_expiration, '$etoile', '$serveur')";
	} else {
		$code = "''";
	}

	$p->code = $code;
	$p->interdire_scripts = false;
	return $p;
}

/**
 * Fonction privée pour le calcul de la balise #COMPTE_RETROGRADE
 *
 * @param string $statut
 * @param string $date
 * @param string $raison
 * @return string
 */
function calculer_balise_compte_retrograde_dist(string $statut, string $date) : string {
	include_spip('inc/comptes_expirables');
	$comptes = new \Spip\ComptesExpirables;
	$statuts_expires = $comptes::get_statuts_expires();

	$est_expire = ((
		// Le statut correspond à un des statuts expirés possibles
		in_array($statut, $statuts_expires)
		// Il y a une date d'expiration et elle est dépassée
		and strlen($date)
		and $date != '0000-00-00 00:00:00'
		and time() > strtotime($date)
	) ? ' ' : '');

	return $est_expire;
}
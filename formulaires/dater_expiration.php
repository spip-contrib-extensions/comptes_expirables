<?php
/**
 * Formulaire pour changer la date d'expiration d'un compte
 *
 * @plugin     Comptes expirables
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Comptes_expirables\Formulaires
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_dater_expiration_saisies_dist($id_auteur) : array {

	$saisies = [
		[
			'saisie' => 'date',
			'options' => [
				'nom'         => 'date_expiration',
				'obligatoire' => '',
				'horaire'     => true,
				'conteneur_class' => 'pleine_largeur',
			],
			'verifier' => [
				'type' => 'date',
				'options' => [
					'normaliser' => 'datetime',
				],
			],
		],
	];

	return $saisies;
}

function formulaires_dater_expiration_identifier_dist($id_auteur) : string {
	return $id_auteur;
}

function formulaires_dater_expiration_charger_dist($id_auteur) : array {
	$expiration = sql_fetsel('date_expiration,raison_expiration', 'spip_auteurs', 'id_auteur = ' . intval($id_auteur));
	$valeurs = [
		'id' => $id_auteur,
		'date_expiration' => $expiration['date_expiration'],
		'raison_expiration' => $expiration['raison_expiration'],
		'editer' => _request('editer'),
	];
	return $valeurs;
}

function formulaires_dater_expiration_verifier_dist($id_auteur) : array {
	$erreurs = [];
	return $erreurs;
}

function formulaires_dater_expiration_traiter_dist($id_auteur) : array {
	$retours = [
		'editable' => true,
	];
	$date_expiration = _request('date_expiration');
	$set = [];

	// Modif
	if (
		_request('enregistrer')
		and $date_expiration
	) {
		include_spip('inc/comptes_expirables');
		$comptes = new \Spip\ComptesExpirables;
		$raison_expiration = $comptes::RAISON_DATE_MANUELLE;
		$set = [
			'date_expiration'   => $date_expiration,
			'raison_expiration' => $raison_expiration,
		];
	// Supprimer
	} elseif (_request('supprimer')) {
		$set = [
			'date_expiration'   => '0000-00-00 00:00:00',
			'raison_expiration' => '',
		];
		set_request('date_expiration', '');
	// Annuler
	} else {
		set_request('date_expiration', '');
	}

	if ($set) {
		include_spip('action/editer_objet');
		objet_modifier('auteur', $id_auteur, $set);
	}

	return $retours;
}
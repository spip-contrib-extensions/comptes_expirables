<?php
/**
 * Formulaire pour filtrer les comptes expirables
 *
 * @plugin     Comptes expirables
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Comptes_expirables\Formulaires
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_filtrer_comptes_expirables_saisies_dist() : array {

	$saisies = [
		[
			'saisie' => 'radio',
			'options' => [
				'nom' => 'cpt_date',
				'label' => _T('comptes_expirables:filtre_date'),
				'data' => [
					''       => _T('comptes_expirables:filtre_toutes'),
					'future' => _T('comptes_expirables:filtre_date_future'),
					'passee' => _T('comptes_expirables:filtre_date_passee'),
					'1'      => _T('comptes_expirables:filtre_date_avec'),
					'-1'     => _T('comptes_expirables:filtre_date_sans'),
				],
			],
		],
		[
			'saisie' => 'radio',
			'options' => [
				'nom' => 'cpt_exp',
				'label' => _T('comptes_expirables:filtre_statut'),
				'data' => [
					''   => _T('comptes_expirables:filtre_tous'),
					'-1' => _T('comptes_expirables:filtre_statuts_expires_non'),
					'1'  => _T('comptes_expirables:filtre_statuts_expires'),
				],
			],
		],
		[
			'saisie' => 'radio',
			'options' => [
				'nom' => 'cpt_retro',
				'label' => _T('comptes_expirables:filtre_retrogrades'),
				'data' => [
					''   => _T('comptes_expirables:filtre_tous'),
					'-1' => _T('comptes_expirables:filtre_retrogrades_non'),
					'1'  => _T('comptes_expirables:filtre_retrogrades_oui'),
				],
			],
		],
		[
			'saisie' => 'radio',
			'options' => [
				'nom' => 'cpt_enligne',
				'label' => _T('comptes_expirables:filtre_en_ligne'),
				'data' => [
					''   => _T('comptes_expirables:filtre_tous'),
					'1'  => _T('comptes_expirables:filtre_en_ligne_oui'),
					'-1' => _T('comptes_expirables:filtre_en_ligne_non'),
				],
			],
		],
		'options' => [
			'texte_submit' => _T('filtrer'),
			// inserer_fin est bogué dans les vieille versions de saisies, décommenter quand dépendance mise à jour
			// En attendant on fait ça dans formulaire_fond
			//'inserer_fin' => '<script async src="' . find_in_path('prive/javascript/filtrer_comptes_expirables.js') . '"></script>',
		]
	];

	return $saisies;
}

function formulaires_filtrer_comptes_expirables_charger_dist() : array {
	$valeurs = [];
	foreach (formulaires_filtrer_comptes_expirables_lister_champs() as $champ) {
		$valeurs[$champ] = _request($champ);
	}
	return $valeurs;
}

function formulaires_filtrer_comptes_expirables_verifier_dist() : array {
	$erreurs = [];
	return $erreurs;
}

function formulaires_filtrer_comptes_expirables_traiter_dist() : array {
	$retours = [
		'editable' => true,
	];
	$redirect = self();
	foreach (formulaires_filtrer_comptes_expirables_lister_champs() as $champ) {
		$redirect = parametre_url($redirect, $champ, _request($champ));
	}
	$retours['redirect'] = $redirect;

	return $retours;
}

function formulaires_filtrer_comptes_expirables_lister_champs() {
	$champs = [];
	foreach (formulaires_filtrer_comptes_expirables_saisies_dist() as $saisie) {
		if (isset($saisie['options']['nom'])) {
			$champs[] = $saisie['options']['nom'];
		}
	}

	return $champs;
}
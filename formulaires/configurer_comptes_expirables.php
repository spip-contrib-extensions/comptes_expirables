<?php
/**
 * Formulaire de configuration du plugin Comptes expirables
 *
 * @plugin     Comptes expirables
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Comptes_expirables\Formulaires
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclaration des saisies du formulaire
 *
 * @return array
 */
function formulaires_configurer_comptes_expirables_saisies_dist() : array {

	include_spip('inc/comptes_expirables');
	$comptes = new \Spip\ComptesExpirables;

	$saisies = [
		// Statuts à appliquer aux comptes rétrogradés
		// Définit les types de comptes expirables
		[
			'saisie' => 'comptes_expirables_statuts',
			'options' => [
				'nom'         => 'statuts',
				'obligatoire' => '',
				'defaut'      => $comptes::$config['statuts'],
				'label'       => _T('comptes_expirables:cfg_champ_statuts_label'),
				'explication' => _T('comptes_expirables:cfg_champ_statuts_explication'),
			],
		],
		// Automatismes
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'automatismes',
				'label' => _T('comptes_expirables:cfg_champ_fieldset_automatismes_label'),
				// 'explication' => _T('comptes_expirables:cfg_champ_fieldset_automatismes_explication'),
			],
			'saisies' => [
				// Poser automatiquement une date d'expiration après N jours d'inactivité
				[
					'saisie' => 'input',
					'options' => [
						'nom'         => 'age_en_ligne_max',
						'label'       => _T('comptes_expirables:cfg_champ_age_en_ligne_max_label'),
						'explication' => _T('comptes_expirables:cfg_champ_age_en_ligne_max_explication'),
						'type'        => 'number',
						'step'        => 1,
					],
				],
				// Poser automatiquement une date d'expiration lors de la création de nouveaux comptes
				[
					'saisie' => 'input',
					'options' => [
						'nom'         => 'age_nouveaux_max',
						'label'       => _T('comptes_expirables:cfg_champ_age_nouveaux_max_label'),
						'explication' => _T('comptes_expirables:cfg_champ_age_nouveaux_max_explication'),
						'type'        => 'number',
						'step'        => 1,
					],
				],
			],
		],
		// Notifications
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom'   => 'notifications',
				'label' => _T('comptes_expirables:cfg_champ_fieldset_notifications_label'),
			],
			'saisies' => [
				// Types de notifs à envoyer
				[
					'saisie' => 'checkbox',
					'options' => [
						'nom'         => 'notifications',
						'defaut'      => $comptes::$config['notifications'],
						'label'       => _T('comptes_expirables:cfg_champ_notifications_label'),
						'explication' => _T('comptes_expirables:cfg_champ_notifications_explication'),
						'data' => [
							'admins' => _T('comptes_expirables:cfg_champ_notifications_admins'),
							'users'  => _T('comptes_expirables:cfg_champ_notifications_users'),
						],
					],
				],
				// Email admins perso
				[
					'saisie' => 'input',
					'options' => [
						'nom'         => 'email_admins',
						'defaut'      => $comptes::$config['email_admins'],
						'label'       => _T('comptes_expirables:cfg_champ_email_admins_label'),
						'explication' => _T('comptes_expirables:cfg_champ_email_admins_explication'),
						'afficher_si' => '@notifications@=="admins"',
					],
				],
			],
		],
	];

	return $saisies;
}

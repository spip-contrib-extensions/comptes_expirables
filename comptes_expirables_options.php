<?php
/**
 * Options du plugin Comptes expirables
 *
 * @plugin     Comptes expirables
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Comptes_expirables\Options
 */

// Ajout d'un statut pour les auteurs
$GLOBALS['liste_des_statuts']['comptes_expirables:statut_7desactive'] = '7desactive';

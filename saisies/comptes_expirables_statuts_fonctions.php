<?php

function saisies_comptes_expirables_lister_statuts() {
	include_spip('inc/filtres_ecrire'); // pour traduire les statuts
	$statuts = [];
	// Tous les statuts, listés en principe du plus au moins
	// On met le statut poubelle en dernier
	$statuts_tous = auteurs_lister_statuts('tous', false);
	$k_desactive = array_search('7desactive', $statuts_tous);
	$k_poubelle  = array_search('5poubelle', $statuts_tous);
	[$statuts_tous[$k_desactive], $statuts_tous[$k_poubelle]] = [$statuts_tous[$k_poubelle], $statuts_tous[$k_desactive]];
	// On exclut à la mains certains statuts
	$statuts_expirables_exclus = ['5poubelle', '7desactive', 'nouveau', 'attente'];
	$statuts_expires_exclus    = ['nouveau', 'attente'];
	$statuts_expirables        = array_diff($statuts_tous, $statuts_expirables_exclus);
	$statuts_expires           = array_diff($statuts_tous, $statuts_expires_exclus);
	foreach ($statuts_expirables as $statut) {
		// On prend les statuts inférieurs
		$k_statut = array_search($statut, $statuts_expires);
		$statuts_expires_moins = array_slice($statuts_expires, $k_statut + 1);
		// On traduit
		$data = [];
		foreach ($statuts_expires_moins as $statut_moins) {
			$data[$statut_moins] = traduire_statut_auteur($statut_moins);
		}
		$statuts[$statut] = [
			'nom'     => traduire_statut_auteur($statut),
			'statuts' => $data,
		];
	}

	return $statuts;
}
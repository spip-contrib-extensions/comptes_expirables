<?php

// Sécurité
if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

/**
 * Cette notification s'exécute quand un compte est désactivé
 *
 * @param string $quoi
 *     Événement de notification
 * @param int $id
 *     id de l'objet en relation avec l'événement
 * @param array $options
 *     Options de notification
 */
function notifications_retrograder_compte_expire_auteur_dist($quoi, $id, $options) {
	// Sujet du mail
	$sujet = _T('comptes_expirables:notification_retrograder_auteur_sujet');
	// Destinataires
	$destinataires = pipeline('notifications_destinataires',
		array(
			'args' => array(
				'quoi'    => $quoi,
				'id'      => $id,
				'options' => $options
			),
			'data' => $options['email'],
		)
	);
	// Modèle
	$texte = recuperer_fond(
		'notifications/retrograder_compte_expire_auteur',
		[
			'email'             => $options['email'],
			'id_auteur'         => $options['id_auteur'],
			'nom'               => $options['nom'],
			'statut'            => $options['statut'],
			'raison_expiration' => $options['raison_expiration'],
		]
	);
	// Go go go
	notifications_envoyer_mails($destinataires, $texte, $sujet);
}

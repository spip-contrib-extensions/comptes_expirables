<?php

// Sécurité
if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

/**
 * Cette notification s'exécute quand des comptes sont désactivés
 *
 * @param string $quoi
 *     Événement de notification
 * @param int $id
 *     id de l'objet en relation avec l'événement
 * @param array $options
 *     Options de notification
 */
function notifications_retrograder_comptes_expires_admins_dist($quoi, $id, $options) {
	// Sujet du mail
	$sujet = _T(
		'comptes_expirables:notification_retrograder_admins_sujet',
		[
			'nb' => count($options['ids_auteurs']),
		]
	);
	// Destinataires
	$destinataires = pipeline('notifications_destinataires',
		array(
			'args' => array(
				'quoi'    => $quoi,
				'id'      => $id,
				'options' => $options
			),
			'data' => $options['email'],
		)
	);
	// Modèle
	$texte = recuperer_fond(
		'notifications/retrograder_comptes_expires_admins',
		[
			'email'       => $options['email'],
			'ids_auteurs' => $options['ids_auteurs'],
		]
	);
	// Go go go
	notifications_envoyer_mails($destinataires, $texte, $sujet);
}

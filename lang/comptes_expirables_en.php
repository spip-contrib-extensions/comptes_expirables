<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

	// B
	'bouton_reactiver' => 'Reactivate the account',
	'bouton_configurer' => 'Configurer les comptes expirables',
	'bouton_programmer' => 'Program',
	'bouton_afficher_auteurs_expirables' => 'Show expirable accounts',
	'bouton_job_retrograder' => 'Demote expired accounts',
	'bouton_job_retrograder_verif' => 'Demote all accounts that have passed their expiration date and without the downgraded status?',
	'bouton_job_verifier' => 'Check expirable accounts',
	'bouton_job_verifier_verif' => 'Set an expiration date for accounts matching the conditions?',

	// C
	'comptes_expirables_titre' => 'Expirable accounts',
	'champ_expiration_label' => 'Expiry',
	'champ_en_ligne_label' => 'Activity',
	'champ_date_expiration_label' => 'Expiration date',
	'champ_date_expiration_explication' => 'Scheduled expiration date for this account',
	'champ_raison_expiration_label' => 'Reason for expiration',
	'champ_statut_label' => 'Status',
	'cfg_champ_statuts_label' => 'Downgraded Status',
	'cfg_champ_statuts_explication' => 'Choose the types of expiring accounts by selecting the statuses to apply to demote expired ones.
	<br>If not specified, accounts are not expirable.',
	'cfg_champ_fieldset_automatismes_label' => 'Automations',
	'cfg_champ_fieldset_automatismes_explication' => 'The following options allow to set expiration dates automatically',
	'cfg_champ_age_nouveaux_max_label' => 'New accounts',
	'cfg_champ_age_nouveaux_max_explication' => 'Automatically set an expiration date for new accounts: time limit in days',
	'cfg_champ_age_en_ligne_max_label' => 'Inactivity',
	'cfg_champ_age_en_ligne_max_explication' => 'Automatically set expiration date for inactive accounts: timeout in days',
	'cfg_champ_fieldset_notifications_label' => 'Notifications',
	'cfg_champ_notifications_label' => 'Notifications',
	'cfg_champ_notifications_explication' => 'Recipients to be notified when accounts expire.',
	'cfg_champ_notifications_admins' => 'Notify administrators',
	'cfg_champ_notifications_users' => 'Notify owners of downgraded accounts',
	'cfg_champ_email_admins_label' => 'Admins emails',
	'cfg_champ_email_admins_explication' => '',
	'cfg_email_field_admins_explanation' => 'Admins emails to notify, separated by commas. By default it is the webmaster\'s email.',
	'cfg_titre_parametrages' => 'Settings',

	// E
	'explication_page_auteurs_expirables' => 'This page displays only the accounts concerned by the expiration dates.

	The selection criteria depends on the plugin configuration.

	{{Downgraded}} = expired status + date passed
	{{Inactive}} = never connected or too long ago',

	// F
	'filtre_en_ligne' => 'Activity',
	'filtre_en_ligne_non' => 'Inactive',
	'filtre_en_ligne_oui' => 'Active',
	'filtre_statut' => 'Status',
	'filtre_statuts_expires' => 'Expired',
	'filtre_statuts_expires_non' => 'Unexpired',
	'filtre_retrogrades' => 'Downgraded',
	'filtre_retrogrades_oui' => 'Downgraded',
	'filtre_retrogrades_non' => 'Un-downgraded',
	'filtre_date' => 'Expiration date',
	'filtre_date_avec' => 'With date',
	'filtre_date_sans' => 'Without date',
	'filtre_date_future' => 'Future date',
	'filtre_date_passee' => 'Passed date',
	'filtre_autres' => 'Others',
	'filtre_tous' => 'All',
	'filtre_toutes' => 'All',
	'filtre_null' => '∅',

	// I
	'info_expiration_future' => 'Expiration scheduled on @date@',
	'info_expiration_passee' => 'Expiration date passed on @date@',
	'info_retrograde' => 'Account demoted on @date@',
	'info_en_ligne_jamais' => '∅',
	'info_en_ligne_maintenant' => 'Today',
	'info_en_ligne_1_jour' => '1 day ago',
	'info_en_ligne_nb_jours' => '@nb@ days ago',
	'info_raison' => 'Reason: @raison@',
	'info_droits_statut' => '@statut@: @droits@',
	'info_droits_statut_0minirezo' => 'full admin rights',
	'info_droits_statut_1comite' => 'connection to back and front',
	'info_droits_statut_6forum' => 'connexion to front',
	'info_droits_statut_7desactive' => 'no connexion',
	'info_droits_statut_nouveau' => 'no connexion',
	'info_droits_statut_5poubelle' => 'account deleted',

	// N
	'notification_retrograder_admins_sujet' => 'Demotion of @nb@ account(s)',
	'notification_retrograder_admins_texte' => 'The following accounts have passed their expiration date, they have been demoted: @ids@',
	'notification_retrograder_admins_notif_auteurs' => 'Their owners have been notified individually by email.',
	'notification_retrograder_admins_reinstituer' => 'They can be manually reinstituted by changing their status in the backoffice: @url_admin@',
	'notification_retrograder_auteur_sujet' => 'Change to your administration rights',
	'notification_retrograder_auteur_greetings' => 'Hello @nom@,',
	'notification_retrograder_auteur_intro' => 'Your admin rights have been changed.',
	'notification_retrograder_auteur_raison' => 'Your administration rights have been modified for the following reason: @reason@.',
	'notification_retrograder_auteur_droits' => 'New rights: ',
	'notification_retrograder_auteur_statut_5poubelle' => 'no more connection to the site, account deleted.',
	'notification_retrograder_auteur_statut_6forum' => 'connection to the front but not to the backoffice.',
	'notification_retrograder_auteur_statut_7desactive' => 'no more connection to the site.',
	'notification_retrograder_auteur_contact' => 'Contact us if you want to regain access.',
	'notification_retrograder_auteur_contact_email' => 'Contact us if you want to regain access : @email@',

	// R
	'raison_date_manuelle' => 'Expiration date set manually',
	'raison_echeance_inactif' => 'Account never logged in',
	'raison_echeance_inactif_jours' => 'Account not logged in for @days@ days',
	'raison_echeance_creation' => 'Expiration of @days@ day(s) after account creation',

	// S
	'statut_7desactive' => 'deactivated',

	// T
	'titre_page_configurer_comptes_expirables' => 'Configure expiring accounts',
	'titre_page_comptes_expirables' => 'Expiring Accounts',
	'titre_droits' => 'Status rights',
];

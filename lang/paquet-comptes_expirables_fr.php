<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

	// C
	'comptes_expirables_description' => 'Ce plugin permet de désactiver automatiquement des comptes à date d’expiration :

-* Dates posées manuellement
-* Dates posées automatiquement N jours après création des comptes ou après N jours d’inactivité',
	'comptes_expirables_nom' => 'Comptes expirables',
	'comptes_expirables_slogan' => 'Poser des dates d’expiration aux comptes',
];

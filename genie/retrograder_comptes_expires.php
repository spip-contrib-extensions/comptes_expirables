<?php
/**
 * Tâche périodique du plugin Comptes expirables
 *
 * @plugin     Comptes expirables
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Comptes_expirables\Cron
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Lancer la désactivation des comptes dont la date d'expiration est dépassée
 *
 * Pour chaque compte désactivable, on programme un tâche one-shot.
 * Celle-ci notifie la personne si c'est configuré, et appelle les pipelines pre et post.
 * Ensuite on envoie une unique notif aux admins récapitulant tous les comptes à désactiver.
 *
 * @param int $t
 *     timestamp
 * @return int|null
 *     - null si rien à faire
 *     - positif si la tâche a été traitée
 *     - négatif si la tâche doit se poursuivre (timestamp de la dernière exécution de la tâche)
 */
function genie_retrograder_comptes_expires_dist($timestamp) {

	include_spip('base/abstract_sql');
	include_spip('inc/comptes_expirables');

	$done = null;
	$comptes = new \Spip\ComptesExpirables;
	$config = $comptes::$config;

	// Tous les comptes avec une date d'expiration dépassée et qui n'ont pas le statut expiré
	if ($auteurs = sql_allfetsel(
		'id_auteur, email, nom, statut, date_expiration, raison_expiration',
		'spip_auteurs',
		[
			// Les comptes non webmestres ayant un des statuts expirables, mais pas expiré
			$comptes->get_where('expirables', null, ['expires' => false]),
			// Avec une date d'expiration dépassée
			$comptes->get_where('date_expiration_passee'),
		]
	)) {

		$ids_auteurs = array_column($auteurs, 'id_auteur');
		$ids_auteurs = array_map('intval', $ids_auteurs);

		// Faire le job en plusieurs fois si nécessaire pour éviter un timeout
		$limite = 100; // Nombre max de comptes à rétrograder en 1 fois
		$delai = 0; // Délai avant relance
		if (count($auteurs) > $limite) {
			$auteurs = array_slice($auteurs, 0, $limite, true);
			$done = -($timestamp - $delai);
		} else {
			$done = 1;
		}

		spip_log("genie / retrograder_comptes_expires : rétrograder " . count($auteurs) . ' compte(s) : n°' . implode(', ', $ids_auteurs), 'comptes_expirables');

		// Lancer la machine
		// Cela lance une notif pour chaque compte rétrogradé
		$retrograder_compte = charger_fonction('retrograder_compte_expire', 'action/');
		foreach ($auteurs as $auteur ) {
			$id_auteur = intval($auteur['id_auteur']);
			$retrograder_compte($id_auteur, $auteur);
		}

		// Notifier les admins
		if (in_array('admins', $config['notifications'])) {
			$notifications = charger_fonction('notifications', 'inc');
			$quoi ='retrograder_comptes_expires_admins';
			$options = [
				'email'       => $config['email_admins'],
				'ids_auteurs' => $ids_auteurs,
			];
			$notifications($quoi, 0, $options);
		}
	}

	return $done;
}
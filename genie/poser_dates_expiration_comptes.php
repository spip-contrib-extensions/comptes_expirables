<?php
/**
 * Tâche périodique du plugin Comptes expirables
 *
 * @plugin     Comptes expirables
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Comptes_expirables\Cron
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Poser des dates d'expiration aux comptes remplissant certains critères
 *
 * - Comptes inactifs pendant trop longtemps
 * - Autres critères plugins via pipeline
 *
 * @note
 * Pour les nouveaux comptes, on pose une date d'expiration via le pipeline pipeline post_insertion
 *
 * @param int $timestamp
 *     timestamp
 * @return int|null
 *     - null si rien à faire
 *     - positif si la tâche a été traitée
 *     - négatif si la tâche doit se poursuivre (nombre fonction du timestamp)
 */
function genie_poser_dates_expiration_comptes_dist($timestamp) {

	include_spip('base/abstract_sql');
	include_spip('inc/comptes_expirables');

	$done         = null;
	$set_auteurs  = [];
	$comptes      = new \Spip\ComptesExpirables;
	$config       = $comptes::$config;
	$date         = new DateTime();
	$date_sql_now = $date->format('Y-m-d H:i:s');

	// Les comptes inactifs depuis trop longtemps ou jamais connectés
	// Si des comptes ont déjà une date d'expiration, on update que si la nouvelle est plus proche
	if ($config['age_en_ligne_max'] > 0) {
		// Select
		$select = [
			'id_auteur',
			'statut',
			'date_expiration',
			'raison_expiration',
			'en_ligne',
			'CASE WHEN en_ligne = ' . sql_quote('0000-00-00 00:00:00') . ' THEN NULL ELSE TIMESTAMPDIFF(HOUR, en_ligne, ' . sql_quote($date_sql_now) . ')/24 END AS age_en_ligne',
		];
		$where = [
			// comptes non webmestres ayant un des statuts expirables, sauf ceux déjà expirés
			$comptes->get_where('expirables', null, ['expires' => false]),
			// inactifs depuis trop longtemps ou jamais connectés
			$comptes->get_where('en_ligne_non', null, ['date' => $date_sql_now]),
			// sans date d'expiration, ou avec mais plus lointaine que celle qu'on va mettre
			$comptes->get_where('date_expiration_non', null, ['date_min' => $date_sql_now]),
		];
		if ($auteurs_inactifs = sql_allfetsel(
			$select,
			'spip_auteurs',
			$where,
			'id_auteur',
			'age_en_ligne DESC'/*,'','','',false*/
		)) {
			foreach ($auteurs_inactifs as $auteur) {
				$jours = round($auteur['age_en_ligne'], 0);
				$raison = ($auteur['age_en_ligne'] ? $comptes::RAISON_ECHEANCE_INACTIF_JOURS . "{jours=$jours}" : $comptes::RAISON_ECHEANCE_INACTIF);
				$set_auteurs[$auteur['id_auteur']] = [
					'date_expiration'   => $date_sql_now,
					'raison_expiration' => $raison,
				];
			}
		}
	}

	// Pipeline permettant aux plugins de compléter le set (minimump date + raison)
	$set_auteurs = pipeline('comptes_expirables_poser_dates_expiration', [
		'args' => [
			'date_sql' => $date_sql_now, // si besoin de mettre la date courante comme date d'expiration
			'config'   => $config, // utile ?
		],
		'data' => $set_auteurs,
	]);

	// On met à jour en plusieurs fois pour éviter les timeouts
	$limite = 100; // Nombre max de comptes à mettre à jour en 1 fois
	$delai = 0; // Délai avant prochaine exécution
	if ($set_auteurs) {
		include_spip('action/editer_objet');
		if (count($set_auteurs) > $limite) {
			$set_auteurs = array_slice($set_auteurs, 0, $limite, true);
			$done = -($timestamp - $delai);
		} else {
			$done = 1;
		}
		foreach ($set_auteurs as $id_auteur => $set) {
			autoriser_exception('modifier', 'auteur', $id_auteur);
			autoriser_exception('instituer', 'auteur', $id_auteur);
			objet_modifier('auteur', $id_auteur, $set);
			autoriser_exception('instituer', 'auteur', $id_auteur, false);
			autoriser_exception('modifier', 'auteur', $id_auteur, false);
		}
		spip_log("genie / poser_dates_expiration_comptes : programmer une expiration le \"$date_sql_now\" pour " . count($set_auteurs) . ' compte(s) : n°' . implode(',', array_keys($set_auteurs)), 'comptes_expirables');
	}

	return $done;
}

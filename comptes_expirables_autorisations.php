<?php
/**
 * Définit les autorisations du plugin Comptes expirables
 *
 * @plugin     Comptes expirables
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Comptes_expirables\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function comptes_expirables_autoriser() {
}

/**
 * Autorisation d'éditer la date d'expiration d'un auteur dans une interface
 *
 * Il faut être soit-même webmestre et que l'auteur :
 * - ait un des statuts configurés ou qu'il ait déjà une date
 * - ne soit pas webmestre
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 *     Infos de l'auteur pour économiser une requête : statut, webmestre, date_expiration
 * @return bool          true s'il a le droit, false sinon
 */
function autoriser_auteur_editer_date_expiration_dist($faire, $type, $id, $qui, $opt) {
	include_spip('inc/comptes_expirables');
	$comptes = new \Spip\ComptesExpirables;
	$config  = $comptes::$config;

	// On prend tous les statuts configurés pour recevoir des dates d'expiration ET le statut mis une fois expiré
	// (il faut pouvoir réinstituer un auteur rétrogradé)
	$statuts_expirables = $comptes::get_statuts_expirables();
	$statuts_expires    = $comptes::get_statuts_expires();
	$statuts_tous       = $comptes::get_statuts();

	// L'auteur concerné
	// On peut donner les infos dans les options pour économiser une requête
	$auteur = ((isset($opt['statut']) and isset($opt['date_expiration']) and isset($opt['webmestre'])) ?
		$opt
		: sql_fetsel('statut,date_expiration,webmestre', 'spip_auteurs', 'id_auteur='.intval($id))
	);
	$auteur_statut_ok     = in_array($auteur['statut'], $statuts_tous);
	$auteur_date_presente = (strlen($auteur['date_expiration']) and $auteur['date_expiration'] !== '0000-00-00 00:00:00');
	$auteur_webmestre     = ($auteur['webmestre'] === 'oui');

	// Celui/celle qui demande l'autorisation
	$self_webmestre       = (($qui['webmestre'] ?? null) === 'oui');

	// Tu peux pas test
	$autoriser = (
		$self_webmestre
		and (
			($auteur_statut_ok or $auteur_date_presente)
			and !$auteur_webmestre
		)
	);

	return $autoriser;
}

/**
 * Autorisation de voir la date d'expiration d'un auteur
 *
 * Idem autorisation pour modifier
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 */
function autoriser_auteur_voir_dateexpiration_dist($faire, $type, $id, $qui, $opt) {
	return autoriser_auteur_editer_date_expiration_dist($faire, $type, $id, $qui, $opt);
}


// ----------------------
// Champs extras : auteur
// ----------------------


/**
 * Autorisation de voir le champ extra `date_expiration` dans le #wysiwyg
 *
 * Personne : on l'affiche ailleurs
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 */
function autoriser_auteur_voirextra_date_expiration_dist($faire, $type, $id, $qui, $opt) {
	return false;
}

/**
 * Autorisation de modifier le champ extra `date_expiration` dans le formulaire d'édition d'un auteur
 *
 * Uniquement les webmestres
 *
 * @note
 * On cache dynamiquement la saisie par le biais d'afficher_si quand les conditions ne sont pas remplies : statut, webmestre.
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 */
function autoriser_auteur_modifierextra_date_expiration_dist($faire, $type, $id, $qui, $opt) {
	$est_webmestre = (($qui['webmestre'] ?? null) === 'oui');
	return $est_webmestre;
}

/**
 * Autorisation de voir le champ extra `raison_expiration` dans le #wysiwyg
 *
 * Personne : on l'affiche ailleurs
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 */
function autoriser_auteur_voirextra_raison_expiration_dist($faire, $type, $id, $qui, $opt) {
	return false;
}

/**
 * Autorisation de modifier le champ extra `raison_expiration` dans le formulaire d'édition d'un auteur
 *
 * Personne : il est géré en arrière plan
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 */
function autoriser_auteur_modifierextra_raison_expiration_dist($faire, $type, $id, $qui, $opt) {
	return false;
}

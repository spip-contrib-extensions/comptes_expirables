<?php
/**
 * Fonctions utiles au plugin Comptes expirables
 *
 * @plugin     Comptes expirables
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Comptes_expirables\Fonctions
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Critères et balises
include_spip('public/comptes_expirables');


/**
 * Formate le code donnant la raison de la désactivation d'un compte
 *
 * Le code reprend le format d'une balise chaîne de langue sans les chevrons, avec éventuellement des paramètres.
 * On le remplace par la vraie chaîne de langue.
 *
 * @example
 * `comptes_expirables:raison_inactif{jours=45}`
 * `comptes_expirables:raison_date_expiration{date=2020-08-10 00:00:00}`
 *
 * @param string $champ
 * @return string
 */
function filtre_afficher_raison_expiration_dist(string $champ) : string {
	$texte = $champ;

	// Extraire la chaîne
	$chaine = (strstr($champ, '{', true) ?: $champ);

	// S'il n'y a pas de préfixe, on ajoute celui du plugin
	if (strpos($chaine, ':') === false) {
		$chaine = "comptes_expirables:$chaine";
	}

	// Extraire les paramètres
	$params = array();
	preg_match('/{[^}]+}/', $champ, $texte_params);
	if (!empty($texte_params[0])) {
		$texte_params[0] = trim($texte_params[0], '{}');
		foreach (explode(',', $texte_params[0]) as $param) {
			$param = explode('=', $param);
			$params[$param[0]] = $param[1];
		}
	}

	$texte = _T($chaine, $params);

	return $texte;
}

/**
 * Retourne le nombre de jours depuis la dernière déconnexion, ou rien si jamais connecté
 *
 * @param string|null $en_ligne
 * @return string
 */
function filtre_afficher_age_en_ligne_dist(? string $en_ligne, ? string $date_reference = null) : string {

	$texte = '';
	$age_en_ligne = null;

	if (
		$en_ligne
		and $en_ligne != '0000-00-00 00:00:00'
	) {
		$date          = new DateTime($date_reference);
		$date_en_ligne = new DateTime($en_ligne);
		$age_en_ligne  = $date->diff($date_en_ligne)->days; // en secondes
	}


	if ($age_en_ligne === null) {
		// $texte = _T('comptes_expirables:info_en_ligne_jamais');
		$texte = '';
	} elseif ($age_en_ligne < 1) {
		$texte = _T('comptes_expirables:info_en_ligne_maintenant');
	} else {
		$texte = singulier_ou_pluriel($age_en_ligne, 'comptes_expirables:info_en_ligne_1_jour', 'comptes_expirables:info_en_ligne_nb_jours');
	}

	return $texte;
}

/**
 * Donne un texte d'explication pour une date d'expiration
 *
 * 3 cas possibles :
 *
 * - future
 * - passée + statut expiré (= compte rétrogradé)
 * - passée + autre statut (ne doit pas arriver en principe : nettoyage auto de la date)
 *
 * @param string $date
 * @param string|null $statut
 * @param string
 *     Texte d'explication
 */
function filtre_expliquer_date_expiration_compte(? string $date, ? string $statut, ? string $raison) : string {
	$explication = '';

	if (
		!empty($date)
		and $date !== '0000-00-00 00:00:00'
	) {

		$texte_date = '<abbr title="'.attribut_html(affdate_heure($date)).'">' . affdate_jourcourt($date) . '</abbr>';

		// Date future
		if (strtotime($date) > time()) {
			$explication = _T('comptes_expirables:info_expiration_future', ['date' => $texte_date]);

		} else {
			include_spip('inc/comptes_expirables');
			$comptes = new \Spip\ComptesExpirables;
			$statuts_expires = $comptes::get_statuts_expires();

			// Date passée + statut expiré = compte rétrogradé
			// FIXME : il faudrait prendre le statut expiré correspondant à l'ancien statut
			if (in_array($statut, $statuts_expires)) {
				$explication = _T('comptes_expirables:info_retrograde', ['date' => $texte_date]);

			// Date passée mais autre statut
			} else {
				$explication = _T('comptes_expirables:info_expiration_passee', ['date' => $texte_date]);
			}
		}

		// Picto pour la raison
		if ($raison) {
			$explication .= ' ' . http_img_pack(
				chemin_image('aide-12.png'),
				'',
				'',
				_T('comptes_expirables:info_raison', ['raison' => $raison])
			);
		}
	}

	return $explication;
}

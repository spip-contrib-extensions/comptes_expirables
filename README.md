# Plugin Comptes expirables pour Spip

Ce plugin permet de poser des dates d'expiration aux comptes utilisateurs.

Lorsqu'une date d'expiration est atteinte, les droits d'administration sont rétrogradés : le statut est baissé pour fermer l'accès à l'espace privé ou à l'espace public.

Un nouveau statut `désactivé` est ajouté pour fermer toute possibilité de connexion.

Des automatismes permettent de poser des dates aux compte inactifs et à tous les nouveaux comptes.

Ce fonctionnement peut être étendu pour compléter la rétrogadation des comptes ou avoir d'autres critères d'automation.


## Configuration

TODO

## Critères

TODO

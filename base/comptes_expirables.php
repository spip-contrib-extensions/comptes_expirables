<?php
/**
 * Déclarations relatives à la base de donnée par Comptes expirables
 *
 * @plugin     Comptes expirables
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Comptes_expirables\Base
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclaration des champs extras
 *
 * - Ajout d'une date d'expiration et d'une raison sur les auteurs
 *
 * @note
 * Les restrictions sont faites sous forme d'autorisations pour tout avoir au même endroit
 *
 * @param array $tables
 * @return array
 */
function comptes_expirables_declarer_champs_extras(array $champs = []) : array {

	include_spip('inc/comptes_expirables');
	$comptes            = new \Spip\ComptesExpirables;
	$config             = $comptes::$config;
	$statuts_expirables = $comptes::get_statuts_expirables();
	$statuts_expires    = $comptes::get_statuts_expires();
	$statuts            = $comptes::get_statuts();
	$data_statuts       = json_encode($statuts);

	// Auteurs : date d'expiration
	$champs['spip_auteurs']['date_expiration'] = [
		'saisie' => 'date',
		'options' => [
			'nom'         => 'date_expiration',
			'label'       => _T('comptes_expirables:champ_date_expiration_label'),
			'explication' => _T('comptes_expirables:champ_date_expiration_explication'),
			'sql'         => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'horaire'     => true,
			// 'attributs'   => "data-statuts=$data_statuts",
			// 'afficher_si' => '@webmestre@ != "oui" && @statut@ IN "' . implode(',', $statuts) . '"', // BUG
		],
		'verifier' => [
			'type' => 'date',
			'options' => [
				'normaliser' => 'datetime',
			],
		],
	];

	// Auteurs : raison de la désactivation d'un compte
	$champs['spip_auteurs']['raison_expiration'] = [
		'saisie' => 'input',
		'options' => [
			'nom'         => 'raison_expiration',
			'label'       => _T('comptes_expirables:champ_raison_expiration_label'),
			'sql'         => "varchar(255) NOT NULL DEFAULT ''",
			'traitements' => 'filtre_afficher_raison_expiration_dist(%s)',
		],
	];

	return $champs;
}

/**
 * Déclaration des tables des objets éditoriaux
 *
 * @param array $tables
 * @return array
 */
function comptes_expirables_declarer_tables_objets_sql(array $tables) : array {
	// $tables['spip_auteurs']['statut_textes_instituer']['7desactive'] = 'comptes_expirables:statut_7desactive';
	$tables['spip_auteurs']['statut_titres']['7desactive'] = 'comptes_expirables:statut_7desactive';
	$tables['spip_auteurs']['statut_images']['7desactive'] = 'auteur-5poubelle-16.png';

	return $tables;
}

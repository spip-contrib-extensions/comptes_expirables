/**
 * Gestion du formulaire de filtrages de la liste des comptes expirables
 *
 * On recharge automatiquement en ajax dès qu'une valeur change.
 * Il s'agit d'une version très simplifiée de ce que fait le plugin ajaxfiltre.
 * Fonctionne uniquement avec des selects et radios simples.
 */
;jQuery(function($) {
	// Au chargement de la page
	$(function() {
		const $form = $('.formulaire_filtrer_comptes_expirables form');
		$form.parent('.formulaire_spip').addClass('autoreload').find('.boutons').hide();
		$form.find('select, input[type=checkbox], input[type=radio]').on('change', function() {
			let params = {};
			const formData = $form.serializeArray();
			// On exclus les params issus #ACTION_FORMULAIRE
			const exclus = ['exec','formulaire_action','formulaire_action_args'];
			$.each(formData, function( index, value ) {
				if (exclus.indexOf(value.name) === -1) {
					params[value.name] = value.value;
				}
			});
			// recharger la liste d'objets
			$('.liste-objets.auteurs.expirables').ajaxReload({args: params});
		});
	});
});
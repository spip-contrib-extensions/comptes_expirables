;jQuery(function($) {
	// Au chargement de la page
	$(function() {
		const $saisie_date     = $('.formulaire_editer_auteur .editer_date_expiration');
		const $saisie_statut   = $('.formulaire_editer_auteur .editer_statut');
		const $input_statut    = $('.formulaire_editer_auteur [name=statut]');
		const $input_webmestre = $('.formulaire_editer_auteur [name=webmestre]');
		const statuts          = $saisie_date.find('input').first().data('statuts') || [];
		const animation        = 200;

		// Déplacer la date d'expiration sous le statut
		$saisie_date.detach().insertAfter($saisie_statut);

		// Masquer la date sous certaines conditions :
		// statut non expirable ou webmestre
		verifier_statut();
		verifier_webmestre();
		$input_statut.change(function() {verifier_statut()});
		$input_webmestre.change(function() {verifier_webmestre()});
		function verifier_statut() {
			if (statuts.indexOf($input_statut.val()) == -1) {
				cacher_date();
			} else {
				montrer_date();
			}
		}
		function verifier_webmestre() {
			if ($input_webmestre.is(':checked')) {
				cacher_date();
			} else {
				montrer_date();
			}
		}
		function cacher_date() {
			// Obligé d'ajouter une classe à la fin car bug au chargement de la page (mais ok on change)
			$saisie_date.slideUp(animation, function(){$(this).addClass("display-none")}).attr("hidden", true);
		}
		function montrer_date() {
			$saisie_date.slideDown(animation).removeClass("display-none").attr("hidden", false);
		}
	});
});
<?php
/**
 * Action : rétrograder un compte expiré
 *
 * @plugin     Comptes expirables
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Comptes_expirables\Cron
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Rétrograder un compte dont la date d'expiration est dépassée
 *
 * On baisse ses droits d'administration en changeant son statut.
 *
 * @param integer|null $id_auteur
 *     Numéro de l'auteur
 * @param array $auteur
 *     Ligne sql de l'auteur si possible (sinon on récupère les infos nous-même)
 * @return bool
 *     - true si le compte a été désactivé,
 *     - false autrement (erreur…)
 */
function action_retrograder_compte_expire_dist(? int $id_auteur, array $auteur = []) : bool {

	// Appel direct depuis une url avec arg = "id_auteur"
	if (is_null($id_auteur)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
		$id_auteur = intval($arg);
	}

	$done = false;

	// Auteur : sans la ligne sql, on récupère ce dont on a besoin
	if (count($auteur) === 0) {
		include_spip('inc/abstract_sql');
		$auteur = sql_fetsel(
			'id_auteur, nom, statut, webmestre, email, date_expiration, raison_expiration',
			'spip_auteurs',
			"id_auteur = $id_auteur"
		);
	}
	$nom               = ($auteur['nom'] ?? null);
	$statut            = ($auteur['statut'] ?? null);
	$webmestre         = ($auteur['webmestre'] ?? null);
	$email             = ($auteur['email'] ?? null);
	$date_expiration   = ($auteur['date_expiration'] ?? null);
	$raison_expiration = ($auteur['raison_expiration'] ?? null);

	// Configuration
	include_spip('inc/comptes_expirables');
	$comptes            = new \Spip\ComptesExpirables;
	$config             = $comptes::$config;
	$statut_expire      = $comptes::get_statut_expire($statut);
	$statuts_expirables = $comptes::get_statuts_expirables(); // (quid si la config a changé entre temps ?)
	$notifier           = in_array('users', $config['notifications']);

	// Vérifications
	if (
		$webmestre !== 'oui' // pas webmestre
		and in_array($statut, $statuts_expirables) // avec un statut expirable
		and strlen($date_expiration) > 0 // avec une date d'expiration dépassée
		and $date_expiration !== '0000-00-00 00:00:00'
		and (strtotime($date_expiration) < time())
	) {
		include_spip('action/editer_objet');
		include_spip('inc/autoriser');

		// On change juste le statut
		$set = [
			'statut' => $statut_expire,
		];

		// Pipeline avant pour compléter le set : vider le mot de passe ou autre.
		$set = pipeline('comptes_expirables_pre_retrograder', [
			'args' => [
				'id_auteur'         => $id_auteur,
				'ancien_statut'     => $statut,
				'statut'            => $statut_expire,
				'date_expiration'   => $date_expiration,
				'raison_expiration' => $raison_expiration,
			],
			'data' => $set,
		]);

		// Go go go
		autoriser_exception('modifier', 'auteur', $id_auteur);
		autoriser_exception('instituer', 'auteur', $id_auteur);
		objet_modifier('auteur', $id_auteur, $set);
		autoriser_exception('instituer', 'auteur', $id_auteur, false);
		autoriser_exception('modifier', 'auteur', $id_auteur, false);

		// Pipeline après
		pipeline('comptes_expirables_post_retrograder', [
			'args' => [
				'id_auteur'         => $id_auteur,
				'ancien_statut'     => $statut,
				'statut'            => $statut_expire,
				'date_expiration'   => $date_expiration,
				'raison_expiration' => $raison_expiration,
			],
			'data' => $set,
		]);

		// Envoi d'une notif
		if ($notifier) {
			$notifications = charger_fonction('notifications', 'inc');
			$quoi ='retrograder_compte_expire_auteur';
			$options = [
				'email'             => $email,
				'id_auteur'         => $id_auteur,
				'statut'            => $statut_expire,
				'nom'               => $nom,
				'date_expiration'   => $date_expiration,
				'raison_expiration' => $raison_expiration,
			];
			$notifications($quoi, $id_auteur, $options);
		}

		$log_statut = "nouveau statut `$statut_expire`";
		$log_notif = ($notifier ? "notification envoyée à `$email`" : 'pas de notification');
		spip_log("action retrograder_compte_expire / auteur $id_auteur : ok, $log_statut, $log_notif", 'comptes_expirables');
	} else {
		spip_log("action retrograder_compte_expire / auteur $id_auteur : erreur ou rien à faire", 'comptes_expirables');
	}

	return $done;
}

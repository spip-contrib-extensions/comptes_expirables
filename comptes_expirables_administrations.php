<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Comptes expirables
 *
 * @plugin     Comptes expirables
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Comptes_expirables\Installation
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Includes pour Champs extras
include_spip('inc/cextras');
include_spip('base/comptes_expirables');

/**
 * Fonction d'installation et de mise à jour du plugin Comptes expirables.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
 */
function comptes_expirables_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	// Create
	cextras_api_upgrade(comptes_expirables_declarer_champs_extras(), $maj['create']);	

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugin Comptes expirables.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
 */
function comptes_expirables_vider_tables($nom_meta_base_version) {

	cextras_api_vider_tables(comptes_expirables_declarer_champs_extras());
	effacer_meta('comptes_expirables');
	effacer_meta($nom_meta_base_version);
}

<?php
/**
 * Utilisations de pipelines par Comptes expirables
 *
 * @plugin     Comptes expirables
 * @copyright  2021
 * @author     Concurrences
 * @licence    GNU/GPL
 * @package    SPIP\Comptes_expirables\Pipelines
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclaration des tâches périodiques
 *
 * @pipeline taches_generales_cron
 * @param array $taches
 * @return array
 */
function comptes_expirables_taches_generales_cron(array $taches) : array {
	// Nombre de passages par jour
	$passages = 2;
	// Poser automatiquement des dates d'expiration aux comptes remplissant certains critères
	$taches['poser_dates_expiration_comptes'] = 24 * 3600 / $passages;
	// Désactiver tous les comptes qui ont une date d'expiration dépassée
	$taches['retrograder_comptes_expires'] = 24 * 3600 / $passages;

	return $taches;
}

/**
 * Ajouter ou modifier des valeurs avant l’enregistrement des données lors de l’édition d’un élément éditorial.
 *
 * Normalisation / safeguards : retirer la date d'expiration dans certains cas.
 * Par exemple quand un compte devient webmestre ou obtient un statut non expirable.
 *
 * Combinaisons autorisées :
 *
 * |                    | date future | date passée |
 * |--------------------|-------------|-------------|
 * | statuts expirables | OK          | -           |
 * | statut expiré      | -           | OK          |
 * | statut autre       | -           | -           |
 * | webmestre          | -           | -           |
 *
 * Voir aussi normalisation dans formulaire_verifier : mise à jour de la raison quand un admin change manuellement la date
 *
 * @note
 * Attention : dans l'action controler, la date n'est pas encore normalisée (c'est un tableau avec date et heure séparées)
 *
 * @pipeline pre_edition
 * @param array $flux
 *     Tableau associatif :
 *     - args : action, table, table_objet, id_objet, data (vide ?), champs (vide ?), (type)
 *     - data : tableau des champs traités, dépend de l'action effectuée. Voir @note
 * @return array
 */
function comptes_expirables_pre_edition(array $flux) : array {

	// Objet édité et action effectuée (controler, modifier ou instituer)
	$table    = ($flux['args']['table'] ?? null);
	$id_objet = ($flux['args']['id_objet'] ?? null);
	$action   = ($flux['args']['action'] ?? null);
	static $data;

	// Auteurs
	if (
		$table === 'spip_auteurs'
		and $id_auteur = intval($id_objet)
		and count($flux['data'])
	) {

		// Temporaire
		// spip_log("pre_edition / $action : " . json_encode($flux['data']), 'comptes_expirables_debug');

		// Pour normaliser on a besoin de tester à la fois le statut et la date d'expiration,
		// mais $flux['data'] ne donne que l'un ou l'autre selon qu'on soit dans modifier ou instituer.
		// Mais l'API passe toujours séquentiellement dans modifier puis dans instituer quelle que soit la modif,
		// ça nous permet de garder une trace de la date d'expiration avant le passage final dans instituer.
		if ($action === 'modifier') {
			$data[$id_auteur]['date_expiration'] = ($flux['data']['date_expiration'] ?? null);
		} elseif ($action === 'instituer') {

			// Valeurs des champs édités
			$webmestre = $flux['data']['webmestre'];
			$statut    = $flux['data']['statut'];
			$date      = $data[$id_auteur]['date_expiration'];
			unset($data[$id_auteur]['date_expiration']); // ménage si appel multiple à objet_modifier

			// Statuts configurés
			include_spip('inc/comptes_expirables');
			$comptes            = new \Spip\ComptesExpirables;
			$statuts_expirables = $comptes::get_statuts_expirables();
			$statuts_expires    = $comptes::get_statuts_expires();
			$statut_expire      = $comptes::get_statut_expire($statut);
			$statuts            = $comptes::get_statuts();

			// Préparer les tests sur la date et le statut
			$maintenant            = time();
			$date_sql_null         = '0000-00-00 00:00:00';
			$is_date               = (strlen($date) and $date !== $date_sql_null);
			$is_date_future        = ($is_date and strtotime($date) > $maintenant);
			$is_date_passee        = ($is_date and strtotime($date) < $maintenant);
			$is_webmestre          = ($webmestre === 'oui');
			$in_statuts_expirables = in_array($statut, $statuts_expirables); // un des statuts expirables
			$in_statuts_expires    = in_array($statut, $statuts_expires); // un des statuts expirés
			$is_statut_expire      = ($statut === $statut_expire); // le statut expiré propre au nouveau statut
			$in_statuts            = in_array($statut, $statuts_expirables);

			// On lance les tests
			if (
				(
					$is_webmestre
					and $log_detail = 'webmestre' // intouchable
				)
				or
				(
					$date !== null
					and !$is_date
					and $log_detail = 'pas de date d’expiration' // pour s'assurer de vider la raison dans ce cas
				)
				or
				(
					$is_date
					and !$in_statuts
					and $log_detail = "statut `$statut` ni expirable, ni expiré" // pas le droit d'avoir une date dans ce cas
				)
				/*
				or
				(
					$is_date_future
					and $is_statut_expire
					and $log_detail = 'date d’expiration future + statut expiré' // car date ineffective dans ce cas
				)
				or
				(
					$is_date_passee
					and !$is_statut_expire
					and $log_detail = 'date d’expiration dépassée + statut non expiré' // car la date n'a pas de sens dans ce cas
				)*/
			) {
				$champs = [
					'date_expiration'   => $date_sql_null,
					'raison_expiration' => '',
				];
				$flux['data'] = array_merge($flux['data'], $champs);
				$log = "vider les champs d’expiration, raison : $log_detail";
				spip_log("pre_edition / $action / auteur $id_auteur : $log", 'comptes_expirables' . _LOG_DEBUG);
			}
		}
	}

	return $flux;
}

/**
 * Agir au moment d'une insertion en base de données d’un élément éditorial.
 *
 * - Nouveaux auteurs : poser une date d'expiration si une échéance auto est configurée
 *
 * @note
 * Attention aux données de $flux['data] ! Elles sont parfois temporaires et incomplètes.
 * On a 2 cas de figure :
 *   1. création directe via l'API objet_inserer() ou auteur_inserer() : ok, on a tout
 *   2. création via le formulaire editer_auteur : pas ok, données incomplètes
 *
 * Dans le 2ème cas ça se fait en plusieurs étapes :
 *   1. l'auteur est créé "vide" avec un statut `5poubelle` temporaire, pas de login, etc.
 *   2. ensuite il est modifié et institué avec le statut définitif et les autres données.
 *
 * En conséquence, dans ce pipeline on ne connaît à peu près rien de l'auteur créé :
 * $flux['data] ne contient presque rien, et ce sont des valeurs vides ou temporaires.
 *
 * Les autres données seront attribuées plus tard, on ne les connaîtra que dans pre / post_edition.
 * MAIS à ce moment on ne sait plus qu'il s'agit d'une création d'auteur :\
 *
 * Dans ce cas on est obligé de poser la date d'expiration quel que soit le statut,
 * et elle sera vidée après dans pre_edition si nécessaire.
 *
 * @pipeline pre_insertion
 * @param array $flux
 *     Tableau associatif :
 *     - args : table
 *     - data : source, login (vide), lang, statut (5poubelle), webmestre (non), imessage
 * @return array
 */
function comptes_expirables_pre_insertion(array $flux) : array {

	$table                    = ($flux['args']['table'] ?? null);
	$statut                   = ($flux['data']['statut'] ?? null);
	$login                    = ($flux['data']['login'] ?? null);
	$webmestre                = ($flux['data']['webmestre'] ?? null);
	$email                    = ($flux['data']['email'] ?? _request('email')); // obligé de _request() si form editer_auteur
	$date_expiration_actuelle = ($flux['data']['date_expiration'] ?? _request('date_expiration') ?: null); // obligé de _request() si form editer_auteur
	$date_expiration_actuelle = ($date_expiration_actuelle === '0000-00-00 00:00:00' ? null : $date_expiration_actuelle);

	if ($table === 'spip_auteurs') {
		include_spip('inc/comptes_expirables');
		$comptes            = new \Spip\ComptesExpirables;
		$config             = $comptes::$config;
		$age_max            = $config['age_nouveaux_max'];
		$date               = new DateTime();
		$date_future        = $age_max ? $date->modify("+ $age_max days") : null;
		$statuts_expirables = $comptes::get_statuts_expirables();

		// Tester si le statut est expirable : on s'accroche au slip, cf. @note
		// Il faut d'abord déterminer si c'est une création via le formulaire editer_auteur :
		// dans ce cas le statut est temporaire et il faut l'ignorer.
		$is_form_editer_auteur = ($statut === '5poubelle'); // Test suffisant ?
		$is_statut_ok = (
			in_array($statut, $statuts_expirables) // on a le vrai statut, qu'on peut tester (appel direct à l'API)
			or $is_form_editer_auteur // mais si c'est le form editer_auteur, c'est un statut `5poubelle` temporaire qu'il faut ignorer
			or $statut === null // si on n'a pas l'info pour une autre raison, on l'ignore aussi, on le connaitra plus tard dans pre_edition
		);

		if (
			// Il n'est pas webmestre
			$webmestre !== 'oui'
			// Il y a une échéance configurée pour les nouveaux comptes
			and $age_max > 0
			// C'est un statut expirable ou on ne le connaît pas (on le vérifiera dans pre_edition)
			and $is_statut_ok
			// Il n'y a pas déjà une date d'expiration posée manuellement,
			// ou alors plus lointaine que celle qu'on va poser.
			and (
				!$date_expiration_actuelle
				or (
					$date_expiration_actuelle
					and strtotime($date_expiration_actuelle) > $date_future->getTimestamp()
				)
			)
		) {
			$date_expiration = $date_future->format('Y-m-d H:i:s');
			$champs = [
				'date_expiration'   => $date_expiration,
				'raison_expiration' => $comptes::RAISON_ECHEANCE_CREATION . "{jours=$age_max}",
			];
			$flux['data'] = array_merge($flux['data'], $champs);
			// Mettre aussi les valeurs dans le _request sinon objet_modifier_champs() va tout effacer par la suite
			foreach ($champs as $champ => $valeur) {
				set_request($champ, $valeur);
			}
			spip_log("pre_insertion : pose automatique de la date d'expiration `$date_expiration` pour le nouveau compte `$email` (échéance : $age_max jours)", 'comptes_expirables' . _LOG_DEBUG);
		}
	}

	return $flux;
}

/**
 * Compléter les vérifications d'un formulaire CVT.
 *
 * - Auteurs : mettre à jour la raison quand la date d'expiration est modifiée manuellement par un admin (mais pas vidée)
 *
 * @pipeline formulaire_verifier
 * @param array $flux
 *     Tableau associatif :
 *     - args : form, args (paramètres passés au formulaire)
 *     - data : tableau des erreurs précédentes
 * @return array
 */
function comptes_expirables_formulaire_verifier(array $flux) : array {

	$form = ($flux['args']['form'] ?? null);
	if (
		$form === 'editer_auteur'
		and empty($flux['data']) // il n'y a pas d'erreurs
	) {
		$id_auteur       = ($flux['args']['args'][0] ?? null);
		$date_expiration = _request('date_expiration');
		if (
			// Il y a une date d'expiration
			$date_expiration
			and $date_expiration !== '0000-00-00 00:00:00'
			// Et elle a été modifiée
			// Ignorer les secondes pour la comparaison, dans la saisie c'est toujours `00`
			and $date_expiration_ancienne = sql_getfetsel('date_expiration', 'spip_auteurs', 'id_auteur=' . intval($id_auteur))
			and substr($date_expiration, 0, 16) !== substr($date_expiration_ancienne, 0, 16)
		) {
			include_spip('inc/comptes_expirables');
			$comptes = new \Spip\ComptesExpirables;
			$raison_expiration = $comptes::RAISON_DATE_MANUELLE;
			set_request('raison_expiration', $raison_expiration);
			$qui = ($GLOBALS['visiteur_session']['nom'] ?? '?') . ' ('. ($GLOBALS['visiteur_session']['id_auteur'] ?? 0) . ')';
			spip_log("formulaire_verifier / auteur $id_auteur: date d’expiration modifiée par $qui, on met à jour la raison", 'comptes_expirables' . _LOG_DEBUG);
		}
	}

	return $flux;
}

/**
 * Compléter ou modifier le résultat de la compilation d’un squelette donné.
 *
 * - Navigation auteurs : ajout d'un lien vers la configuration du plugin
 * - Liste d'auteurs : ajout d'une colonne pour les dates d'expiration
 *
 * @param array $flux
 *     Tableau associatif :
 *     - args : fond, contexte, options, connect
 *     - data : texte (contenu HTML du squelette)
 * @return array
 */
function comptes_expirables_recuperer_fond(array $flux) : array {

	$fond     = ($flux['args']['fond'] ?? null);
	$contexte = ($flux['args']['contexte'] ?? null);

	switch ($fond) {

		// Navigation de la page auteurs
		case 'prive/squelettes/navigation/auteurs':
			if (autoriser('configurer', '_comptes_expirables')) {
				$ajout = recuperer_fond('prive/squelettes/inclure/navigation_auteurs_expirables');
				// On essaye de le placer dans la boîte des raccourcis, sinon à la fin
				/*if ($texte = preg_replace('/<span\s+class=["\']icone[^\'"]+auteur/i', "${ajout}$0", $flux['data']['texte'], 1)) {
					$flux['data']['texte'] = $texte;
				} else {
					$flux['data']['texte'] .= $ajout;
				}*/
				$flux['data']['texte'] .= $ajout;
			}
			break;

		// Liste d'auteurs
		// Nb : désactivé, ça fonctionne mais on fait une liste séparée plutôt
		/*
		case 'prive/objets/liste/auteurs':
			// Les lignes
			// On se base sur la cellule "id", et on fait une seule requête pour limiter la casse
			if (
				autoriser('voir', 'comptes_expirables')
				and preg_match_all('/<td\s+class=["\']id[\'"]>\s*<a[^>]+>\s*(\d+)/i', $flux['data']['texte'], $match_ids_auteurs) !== false
				and $dates_expirations = sql_allfetsel('date_expiration', 'spip_auteurs', sql_in('id_auteur', $match_ids_auteurs[1]), 'FIELD(id_auteur,'.implode(',', $match_ids_auteurs[1]).')')
			) {
				foreach ($match_ids_auteurs[1] as $k => $id_auteur) {
					$date_expiration = ($dates_expirations[$k]['date_expiration'] === '0000-00-00 00:00:00' ? null : $dates_expirations[$k]['date_expiration']);
					$td = '<td class="expiration secondaire">'
						. ($date_expiration ? '<span class="date_expiration" title="'.attribut_html(affdate_heure($date_expiration)).'">'.affdate($date_expiration).'</span>' : '')
						. '</td>';
						$flux['data']['texte'] = str_replace($match_ids_auteurs[0][$k], $td.$match_ids_auteurs[0][$k], $flux['data']['texte']);
				}
				// Puis l'entête
				$th = '<th class="expiration">'._T('comptes_expirables:champ_expiration_label').'</th>';
				$flux['data']['texte'] = preg_replace('/<th\s+class=["\']id[\'"]/i', "${th}$0", $flux['data']['texte']);
			}
			break;
		*/
	}

	return $flux;
}

/**
 * Afficher des informations sur les objets dans les boîtes infos de l’espace privé. 
 *
 * @pipeline boite_infos
 * @param array $flux
 * @return array
 */
function comptes_expirables_boite_infos(array $flux) : array {

	$objet = ($flux['args']['type'] ?? null);
	$id_objet = ($flux['args']['id'] ?? null);

	// Info date expiration sur les auteurs
	if (
		$objet === 'auteur'
		and $id_objet
		and autoriser('webmestre')
	) {
		$contexte = [
			'id_auteur' => $id_objet,
		];
		$texte = recuperer_fond('prive/squelettes/inclure/boite_infos_auteur_expirable', $contexte);

		// On essaie de se placer sous le numéro, sinon à la fin
		if ($replace = preg_replace('/<div class=["\']statut["\']/i', "$texte$0", $flux['data'], 1)) {
			$flux['data'] = $replace;
		} else {
			$flux['data'] .= $texte;
		}
	}

	return $flux;
}

/**
 * Modifier le résultat de la compilation des squelettes des formulaires
 *
 * - editer_auteur :
 *   - ajouter un JS pour déplacer la saisie date d'expiration + afficher_si maison
 *   - ajouter un attribut data pour le afficher_si
 * - filtrer_comptes_expirables : ajout d'un JS
 *
 * @param array $flux
 *     Tableau associatif :
 *     - args : form, args, je_suis_poste, contexte
 *     - data : contenu HTML
 * @return array
 */
function comptes_expirables_formulaire_fond(array $flux) : array {
	$form = ($flux['args']['form'] ?? null);

	// Formulaire editer auteur
	if (
		$form === 'editer_auteur'
		and $cherche_date = 'id="champ_date_expiration_date"' // saisie date avec heure=oui
		and strpos($flux['data'], $cherche_date) > 0
	) {
		include_spip('inc/comptes_expirables');
		$comptes            = new \Spip\ComptesExpirables;
		$config             = $comptes::$config;
		$statuts_expirables = $comptes::get_statuts_expirables();
		$statuts_expires    = $comptes::get_statuts_expires();
		$statuts_tous       = $comptes::get_statuts();
		$id_auteur          = ($flux['args']['args'][0] ?? 0);
		$is_new             = !intval($id_auteur);
		$data_statuts       = json_encode($is_new ? $statuts_expirables : $statuts_tous);
		$ajouter            = " data-statuts=$data_statuts ";
		// Attribut data-statuts sur l'input de date
		$flux['data']       = substr_replace(
			$flux['data'],
			$ajouter,
			strpos($flux['data'], $cherche_date),
			0
		);
		// Script JS
		$flux['data'] .= '<script async src="'.find_in_path('prive/javascript/comptes_expirables_editer_auteur.js').'"></script>';

	// Formulaire de filtrage
	} elseif ($form === 'filtrer_comptes_expirables') {
		$flux['data'] .= '<script async src="' . find_in_path('prive/javascript/filtrer_comptes_expirables.js') . '"></script>';
	}

	return $flux;
}
